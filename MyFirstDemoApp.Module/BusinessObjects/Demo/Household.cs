﻿using System;
using System.Linq;
using System.Text;
using DevExpress.Xpo;
using DevExpress.ExpressApp;
using System.ComponentModel;
using DevExpress.ExpressApp.DC;
using DevExpress.Data.Filtering;
using DevExpress.Persistent.Base;
using System.Collections.Generic;
using DevExpress.ExpressApp.Model;
using DevExpress.Persistent.BaseImpl;
using DevExpress.Persistent.Validation;

namespace MyFirstDemoApp.Module.BusinessObjects.Demo
{
    [DefaultClassOptions]
    public class Household : XPObject
    {
        public Household(Session session)
            : base(session)
        {
        }

        public override void AfterConstruction()
        {
            base.AfterConstruction();
            
        }

        private string _name;
        private string _address;
        private string _city;
        private string _phoneNumber;
        private string _email;
        private string _description;

        public string Name
        {
            get { return _name; }
            set { SetPropertyValue("Name", ref _name, value); }
        }
        public string Address
        {
            get { return _address; }
            set { SetPropertyValue("Address", ref _address, value); }
        }
        public string City
        {
            get { return _city; }
            set { SetPropertyValue("City", ref _city, value); }
        }
        public string PhoneNumber
        {
            get { return _name; }
            set { SetPropertyValue("PhoneNumber", ref _phoneNumber, value); }
        }
        public string Email
        {
            get { return _email; }
            set { SetPropertyValue("Email", ref _email, value); }
        }

        [Size(2048)]
        public string Description
        {
            get { return _description; }
            set { SetPropertyValue("Description", ref _description, value); }
        }
        
        [Association]
        public XPCollection<Person> People
        {
            get
            {
                return GetCollection<Person>("People");
            }
        }

        [Association]
        public XPCollection<Tag> Tags
        {
            get
            {
                return GetCollection<Tag>("Tags");
            }
        }

        private XPCollection<AuditDataItemPersistent> _auditTrail;
        public XPCollection<AuditDataItemPersistent> AuditTrail
        {
            get
            {
                if (_auditTrail == null)
                    _auditTrail = AuditedObjectWeakReference.GetAuditTrail(Session, this);
                return _auditTrail;
            }
        }
    }
}