﻿using System;
using System.Linq;
using System.Text;
using DevExpress.Xpo;
using DevExpress.ExpressApp;
using System.ComponentModel;
using DevExpress.ExpressApp.DC;
using DevExpress.Data.Filtering;
using DevExpress.Persistent.Base;
using System.Collections.Generic;
using DevExpress.ExpressApp.Model;
using DevExpress.Persistent.BaseImpl;
using DevExpress.Persistent.Validation;

namespace MyFirstDemoApp.Module.BusinessObjects.Demo
{
    [DefaultClassOptions]
    public class Person : XPObject
    { 
        public Person(Session session)
            : base(session)
        {
        }
        public override void AfterConstruction()
        {
            base.AfterConstruction();
            
        }

        private string _firstName;
        private string _lastName;
        private DateTime _dob;
        private string _email;
        private string _phoneNumber;

        private Household _household;

        public string FirstName
        {
            get { return _firstName; }
            set { SetPropertyValue("FirstName", ref _firstName, value); }
        }
        public string LastName
        {
            get { return _lastName; }
            set { SetPropertyValue("LastName", ref _lastName, value); }
        }

        [NonPersistent]
        public string Name
        {
            get { return string.Format("{0} {1}", FirstName ?? "", LastName ?? "")  ; }
        }

        public DateTime Dob
        {
            get { return _dob; }
            set { SetPropertyValue("Dob", ref _dob, value); }
        }
        
        public string PhoneNumber
        {
            get { return _phoneNumber; }
            set { SetPropertyValue("PhoneNumber", ref _phoneNumber, value); }
        }
        public string Email
        {
            get { return _email; }
            set { SetPropertyValue("Email", ref _email, value); }
        }

        [Association]
        public Household Household
        {
            get { return _household; }
            set { SetPropertyValue("Household", ref _household, value); }
        }
        
        
    }
}